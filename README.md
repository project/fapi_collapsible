CONTENTS OF THIS FILE
---------------------

* Introduction
* Recommended modules
* Installation
* Usage example
* Maintainers

INTRODUCTION
-------------------
This module provides a form element custom for create collapsible element (with FAPI) with accessibility

RECOMMENDED MODULES
-------------------
* You can use this module with [Entity list](https://www.drupal.org/project/entity_list) for render filter collapsible.

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
  for further information.

USAGE EXAMPLE
-----
```php
$form['author'] = [
  '#type' => 'collapsible',
  '#title' => $this->t('Author'),
];

$form['author']['name'] = [
  '#type' => 'textfield',
  '#title' => $this->t('Name'),
];
```

MAINTAINERS
-----------

Current maintainers:
* Alexis Iannone (Anwoon) - https://www.drupal.org/u/anwoon

This project has been sponsored by:
* Ecedi
