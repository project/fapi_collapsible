<?php

namespace Drupal\fapi_collapsible\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Fieldset;

/**
 * Provides a render element for group of form elements with collapsible action.
 *
 * @see \Drupal\Core\Render\Element\Fieldgroup
 * @see \Drupal\Core\Render\Element\Details
 *
 * @RenderElement("collapsible")
 */
class Collapsible extends Fieldset {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();

    $info['#theme_wrappers'] = ['collapsible'];
    $info['#process'][] = [static::class, 'processCollapsible'];
    $info['#expanded'] = FALSE;
    $info['#name'] = 'field';
    $info['#id_collapsible'] = '';
    $info['#description_attributes'] = [];
    $info['#description'] = '';

    return $info;
  }

  /**
   * Add js of collapsible action.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processCollapsible(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['#attached']['library'][] = 'fapi_collapsible/collapsible';
    return $element;
  }

}
