/**
 * @file
 * Script for collapsible elements.
 *
 * Uses Drupal.behaviors.
 *
 * @see customer/README.md
 * @see https://www.drupal.org/docs/8/api/javascript-api/javascript-api-overview
 */

(function ($, Drupal) {
  Drupal.behaviors.collapsible = {
    attach(context) {
      // Use the jQuery once plugin to execute code once.
      once('collapsible', 'body').forEach(function () {
        const $body = $('body');

        // Toggle on click on the button.
        $body.on('click', '.js-collapsible-cta', function (event) {
          const $cta = $(this);
          const $parent = $cta.closest('.js-collapsible');
          const rel = $parent.attr('data-collapsible-rel');
          let $others;

          if ($cta.attr('aria-expanded') === 'false') {
            if (rel) {
              $others = $(`[data-collapsible-rel="${rel}"].is-expanded`);
              $others
                .find('.js-collapsible-cta')
                .first()
                .attr('aria-expanded', 'false');
              $others.removeClass('is-expanded');
              $others.trigger('collapsible-close');
            }

            $cta.attr('aria-expanded', 'true');
            $parent.addClass('is-expanded');

            // Custom event.
            $parent.trigger('collapsible-open');
          } else {
            $cta.attr('aria-expanded', 'false');
            $parent.removeClass('is-expanded');

            // Custom event.
            $parent.trigger('collapsible-close');
          }
        });

        // Close on click somewhere else.
        $body.on('click focusin', function (event) {
          const $target = $(event.target);
          // Maybe the click is somewhere inside a collapsible.
          const $parent = $target.parents('.js-collapsible');

          // For all open collapsibles, but not the one where the click happens.
          $('.js-collapsible.is-expanded')
            .not($parent)
            .each(function () {
              const $collapsible = $(this);

              // Only if their data-collapsible-close==true.
              if ($collapsible.attr('data-collapsible-close') === 'true') {
                const $cta = $collapsible.find('.js-collapsible-cta');

                // Close them.
                $cta.attr('aria-expanded', 'false');
                $collapsible.removeClass('is-expanded');

                // Custom event.
                $collapsible.trigger('collapsible-close');
              }
            });
        });
      });
    },
  };
})(jQuery, Drupal);
